Contributions are welcome!
--------------------------
Thank you for your interest in contributing to InspectorCell! There are several
ways to help in the development of InspectorCell:

* If you found a bug or mistakes in our documentation, please feel free to open
  an issue with an `bug` label in our `gitlab repository <https://gitlab.org/InspectorCell/InspectorCell.git>`_
* If have an idea for a feature, changes or improvements, please opening an 
  issue with an `request` label at our gitlab
* If you want to implement something yourselve, feel free to fork our
  repository, and create a new branch. Afterwards we will gladly look into
  your pull request. If you have any troubles in the process, don't hasitate
  to contact us

Contributor license agreement
-----------------------------
If you submit any original work of authorship (Contrribution), including any
modifications or additions to InspectorCell, you grant InspectorCell a
perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
copyright license to reproduce, prepare derivative works of,
publicly display, publicly perform, sublicense, and distribute your
Contribution and derivatives of your Contribution.
